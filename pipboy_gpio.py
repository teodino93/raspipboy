import RPi.GPIO as GPIO
from Queue import Queue
import RotaryEncoder

GPIO.setmode(GPIO.BCM)

encA_PIN  = 14
encB_PIN  = 15
encSW_PIN = 23

RotQueue = Queue()
encoder = RotaryEncoder.RotaryEncoderWorker(encA_PIN, encB_PIN, encSW_PIN, RotQueue)

rotA_PIN = 2
rotB_PIN = 3
rotC_PIN = 4
rotD_PIN = 17
rotE_PIN = 27
rotF_PIN = 22
rotG_PIN = 21
rotH_PIN = 20
rotI_PIN = 13
rotL_PIN = 19
rotM_PIN = 26
rotN_PIN = 16


GPIO.setup(rotA_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(rotB_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(rotC_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(rotD_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(rotE_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(rotF_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(rotG_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(rotH_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(rotI_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(rotL_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(rotM_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(rotN_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

import atexit
@atexit.register
def close_gpio():                                                               # close the gpio ports at exit ti$
    encoder.Exit()
